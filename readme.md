# Simple logger

## Usage

Use it as regular [LoggerInterface](vendor/psr/log/Psr/Log/LoggerInterface.php).

```php
$crashHandler = new LoggerCrashEmailHandler('exception@example.com'); #send email if smth is wrong
$logger = new Logger(
    'debug', #log level
    $crashHandler, #error handler
    true, #is buffering
    250, #buffer size
    'emergency' #log level to write immediate msg despite on buffering
);

// add as many logger writers as you want
//$logger->addLogger($grayLog);
//$logger->addLogger($monolog);

$logger->setBufferingMode(false);
$logger->error('Error msg', ['error_code' => 999]); #write error

$logger->setBufferingMode(true);
$logger->error('Error msg', ['error_code' => 999]); #this one is defered
$logger->error('Error msg', ['error_code' => 999]); #this one is defered
$logger->critical('Critical msg', ['error_code' => 999]); #this one is defered and persisted at the same time
$logger->flush('End of errors'); #persist
```

## next steps
* TESTS!!!
