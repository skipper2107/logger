<?php
namespace Skipper\Logger\Contracts;

use Skipper\Logger\Exceptions\LoggerIsNotResponding;

interface LoggerErrorReporter
{
    /**
     * @param LoggerIsNotResponding $exception
     * @param Writable $logger
     * @return void
     */
    public function report(LoggerIsNotResponding $exception, Writable $logger): void;
}