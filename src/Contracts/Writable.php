<?php
namespace Skipper\Logger\Contracts;

use Skipper\Logger\Exceptions\LoggerIsNotResponding;
use Skipper\Logger\LogMessage;

interface Writable
{
    /**
     * @param LogMessage $data
     * @return void
     * @throws LoggerIsNotResponding
     */
    public function write(LogMessage $data): void;
}