<?php
namespace Skipper\Logger;

use Psr\Log\AbstractLogger;
use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;
use Skipper\Exceptions\DomainException;
use Skipper\Logger\Contracts\LoggerErrorReporter;
use Skipper\Logger\Contracts\Writable;
use Skipper\Logger\Exceptions\InvalidLogLevel;
use Skipper\Logger\Exceptions\LoggerIsNotResponding;

class Logger extends AbstractLogger implements LoggerInterface, Writable
{
    private const PRIORITY_MAP = [
        LogLevel::DEBUG,
        LogLevel::INFO,
        LogLevel::NOTICE,
        LogLevel::WARNING,
        LogLevel::ERROR,
        LogLevel::CRITICAL,
        LogLevel::ALERT,
        LogLevel::EMERGENCY,
    ];

    private const KEY = 'key';
    private const LEVEL = 'level';
    private const DATA = 'data';

    /** @var Writable[] $loggers */
    private $loggers = [];

    /** @var string $immediateLogLevel */
    private $immediateLogLevel = LogLevel::ERROR;

    /** @var string $logLevel */
    private $logLevel;

    /** @var bool $isBuffering */
    private $isBuffering;

    /** @var array $buffer */
    private $buffer = [];

    /** @var int $bufferMaxSize */
    private $bufferMaxSize;

    /** @var string $persistLevel */
    private $persistLevel;

    /** @var string $logKey */
    private $logKey;

    /** @var $errorHandler LoggerErrorReporter */
    private $errorHandler;

    public function __construct(
        string $logLevel,
        LoggerErrorReporter $errorReporter,
        bool $isBuffering = true,
        int $maxBufferSize = 500,
        string $emergencyLogLevel = LogLevel::ERROR
    ) {
        $this->logLevel = $logLevel;
        $this->persistLevel = $logLevel;
        $this->isBuffering = $isBuffering;
        $this->bufferMaxSize = $maxBufferSize;
        $this->immediateLogLevel = $emergencyLogLevel;
        $this->errorHandler = $errorReporter;
        $this->generateLogKey();
    }

    /**
     * @param null|string $default
     * @return Logger
     */
    public function generateLogKey(?string $default = null): Logger
    {
        $this->logKey = $default ?? bin2hex(openssl_random_pseudo_bytes(10));

        return $this;
    }

    /**
     * @param Writable $writable
     * @return Logger
     */
    public function addLogger(Writable $writable): Logger
    {
        $this->loggers[] = $writable;

        return $this;
    }

    /**
     * @param \Exception $e
     * @return Logger
     * @throws InvalidLogLevel
     */
    public function exception(\Exception $e): Logger
    {
        $data = [];
        $errors = [];
        if ($e instanceof DomainException) {
            $data = $e->getData();
            foreach ($e->getErrors() as $error) {
                $errors[] = [
                    'msg' => $error->getMsg(),
                    'reason' => $error->getReason(),
                    'location' => $error->getLocation(),
                ];
            }
        }

        $this->error(sprintf('Error logged: %s', $e->getMessage()), [
            'exception' => get_class($e),
            'msg' => $e->getMessage(),
            'code' => $e->getCode(),
            'file' => $e->getFile(),
            'errors' => $errors,
            'line' => $e->getLine(),
            'trace' => $e->getTrace(),
            'data' => $data,
        ]);

        return $this;
    }

    /**
     * System is unusable.
     *
     * @param string $message
     * @param array $context
     *
     * @return void
     * @throws InvalidLogLevel
     */
    public function emergency($message, array $context = [])
    {
        $this->log(LogLevel::EMERGENCY, $message, $context);
    }

    /**
     * Logs with an arbitrary level.
     *
     * @param mixed $level
     * @param string $message
     * @param array $context
     * @return void
     * @throws InvalidLogLevel
     */
    public function log($level, $message, array $context = [])
    {
        $this->assertValidLogLevel($level);
        if ($this->isSkipLevel($level)) {
            return;
        }
        if ($this->isBuffering) {
            $this->addToBuffer($level, $message, $context);
            if ($this->isEmergency($level)) {
                $this->write($this->buildLogMsg($level, $message, $context));
            }
        } else {
            $this->write($this->buildLogMsg($level, $message, $context));
        }
    }

    /**
     * @param string $level
     * @throws InvalidLogLevel
     */
    private function assertValidLogLevel(string $level): void
    {
        if (false === $this->isValidLogLevel($level)) {
            throw new InvalidLogLevel([
                'log_level' => $level,
            ]);
        }
    }

    /**
     * @param string $level
     * @return bool
     */
    private function isValidLogLevel(string $level): bool
    {
        return in_array($level, $this->getLogLevels());
    }

    /**
     * @return array
     */
    private function getLogLevels(): array
    {
        return self::PRIORITY_MAP;
    }

    /**
     * @param $level string
     * @return bool
     */
    private function isSkipLevel(string $level): bool
    {
        return $this->isLevelHasMorePriority($this->logLevel, $level);
    }

    /**
     * @param string $current
     * @param string $compared
     * @return bool
     */
    private function isLevelHasMorePriority(string $current, string $compared): bool
    {
        $currentKey = array_search($current, self::PRIORITY_MAP);
        $comparedKey = array_search($compared, self::PRIORITY_MAP);

        return $comparedKey >= $currentKey;
    }

    /**
     * @param string $level
     * @param string $msg
     * @param array $additional
     * @return Logger
     */
    public function addToBuffer(string $level, string $msg, array $additional = []): Logger
    {
        if (false === $this->isBuffering) {
            return $this;
        }
        $this->buffer[(string)microtime(true)] = $this->buildLogMsg($level, $msg, $additional)->toArray();
        if ($this->isFullBuffer()) {
            $this->flushBuffer('-Buffer overloaded-');
        }

        return $this;
    }

    /**
     * @param string $level
     * @param string $msg
     * @param array $additional
     * @return LogMessage
     */
    private function buildLogMsg(string $level, string $msg, array $additional = []): LogMessage
    {
        $file = $this->extractFromPayload('file', $additional, null);
        $line = $this->extractFromPayload('line', $additional, null);

        return new LogMessage($level, $msg, $this->logKey, $file, $line, $additional);
    }

    /**
     * @param string $key
     * @param array $data
     * @param null $default
     * @return mixed|null
     */
    private function extractFromPayload(string $key, array &$data = [], $default = null)
    {
        $value = $default;
        if (array_key_exists($key, $data)) {
            $value = $data[$key];
            unset($data[$key]);
        }

        return $value;
    }

    /**
     * @return bool
     */
    private function isFullBuffer(): bool
    {
        return $this->isBuffering && count($this->buffer) >= $this->bufferMaxSize;
    }

    /**
     * @param string $msg
     */
    private function flushBuffer(string $msg): void
    {
        if (count($this->buffer) < 1) {
            return;
        }

        $this->write($this->buildLogMsg($this->logLevel, $msg, $this->buffer));
        $this->buffer = [];
    }

    /**
     * @param LogMessage $data
     * @return void
     */
    public function write(LogMessage $data): void
    {
        foreach ($this->loggers as $logger) {
            try {
                $logger->write($data);
            } catch (LoggerIsNotResponding $e) {
                $this->errorHandler->report($e, $logger);
            }
        }
    }

    /**
     * @param string $level
     * @return bool
     */
    private function isEmergency(string $level): bool
    {
        return $this->isLevelHasMorePriority($this->immediateLogLevel, $level);
    }

    /**
     * @param bool $isBuffering
     * @return Logger
     */
    public function setBufferingMode(bool $isBuffering = true): Logger
    {
        $this->isBuffering = $isBuffering;
        if (false === $this->isBuffering()) {
            $this->flushBuffer('-Stop Buffering-');
        }

        return $this;
    }

    /**
     * @return bool
     */
    public function isBuffering(): bool
    {
        return $this->isBuffering;
    }

    /**
     * @param string $msg
     * @param array $data
     * @return Logger
     * @throws InvalidLogLevel
     */
    public function flush(string $msg, array $data = []): Logger
    {
        $this->persist($msg, $data);
        $this->flushBuffer($msg);

        return $this;
    }

    /**
     * @param string $msg
     * @param array $data
     * @return Logger
     * @throws InvalidLogLevel
     */
    public function persist(string $msg, array $data = []): Logger
    {
        $this->log($this->persistLevel, $msg, $data);

        return $this;
    }

    /**
     * @return array
     */
    public function getBuffer(): array
    {
        return $this->buffer;
    }

    /**
     * @return bool
     */
    public function isNotEmptyBuffer(): bool
    {
        return $this->isBuffering && (bool)count($this->buffer);
    }
}