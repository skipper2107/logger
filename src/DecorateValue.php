<?php
namespace Skipper\Logger;

trait DecorateValue
{
    /**
     * @param $value
     * @return mixed
     */
    protected function decorate($value)
    {
        return var_export($value, true);
    }
}