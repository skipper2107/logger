<?php
namespace Skipper\Logger;

class LogMessage
{
    /** @var string $level */
    private $level;

    /** @var string $key */
    private $key;

    /** @var string $msg */
    private $msg;

    /** @var array $data */
    private $data;

    /** @var $file string|null */
    private $file;

    /** @var $line int|null */
    private $line;

    public function __construct(string $level, string $msg, string $key, ?string $file, ?int $line, array $data)
    {
        $this->level = $level;
        $this->msg = $msg;
        $this->key = $key;
        $this->data = $data;
        $this->file = $file;
        $this->line = $line;
    }

    /**
     * @return null|string
     */
    public function getFile(): ?string
    {
        return $this->file;
    }

    /**
     * @return int|null
     */
    public function getLine(): ?int
    {
        return $this->line;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @return string
     */
    public function getKey(): string
    {
        return $this->key;
    }

    /**
     * @return string
     */
    public function getLevel(): string
    {
        return $this->level;
    }

    /**
     * @return string
     */
    public function getMsg(): string
    {
        return $this->msg;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'level' => $this->level,
            'msg' => $this->msg,
            'key' => $this->key,
            'data' => $this->data,
            'file' => $this->file,
            'line' => $this->line,
        ];
    }
}