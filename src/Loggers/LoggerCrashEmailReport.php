<?php
namespace Skipper\Logger\Loggers;

use Skipper\Logger\Contracts\LoggerErrorReporter;
use Skipper\Logger\Contracts\Writable;
use Skipper\Logger\Exceptions\LoggerIsNotResponding;

class LoggerCrashEmailReport implements LoggerErrorReporter
{
    /** @var string $email */
    protected $email;

    /**
     * LoggerCrashEmailReport constructor.
     * @param string $email
     */
    public function __construct(string $email)
    {
        $this->email = $email;
    }

    /**
     * @param LoggerIsNotResponding $exception
     * @param Writable $logger
     * @return void
     */
    public function report(LoggerIsNotResponding $exception, Writable $logger): void
    {
        $format = <<<msg
Logger %s crashed at %s.
Msg: %s.
msg;
        $msg = sprintf($format, get_class($logger), date('Y-m-d H:i:s'), $exception->getMessage());
        mail($this->email, 'LoggerCrashed', $msg);
    }
}