<?php
namespace Skipper\Logger\Loggers;

use Monolog\Logger;
use Skipper\Logger\Contracts\Writable;
use Skipper\Logger\LogMessage;

class Monolog implements Writable
{
    /**
     * @var Logger
     */
    private $logger;

    public function __construct(Logger $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param LogMessage $data
     * @return void
     */
    public function write(LogMessage $data): void
    {
        $this->logger->log($data->getLevel(), $data->getMsg(), $data->toArray());
    }
}