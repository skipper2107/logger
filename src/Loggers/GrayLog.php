<?php
namespace Skipper\Logger\Loggers;

use Gelf\Message;
use Gelf\Publisher;
use Gelf\Transport\AbstractTransport;
use Gelf\Transport\TcpTransport;
use Gelf\Transport\UdpTransport;
use Skipper\Logger\Contracts\Writable;
use Skipper\Logger\DecorateValue;
use Skipper\Logger\Exceptions\LoggerException;
use Skipper\Logger\Exceptions\LoggerIsNotResponding;
use Skipper\Logger\LogMessage;

class GrayLog implements Writable
{
    use DecorateValue;

    public const CONNECTION_TYPE_TCP = 'TCP';
    public const CONNECTION_TYPE_UDP = 'UDP';

    /**
     * @var AbstractTransport
     */
    protected $connection;

    /**
     * @var Publisher
     */
    protected $publisher;

    /**
     * GrayLog constructor.
     * @param string $host
     * @param int $port
     * @param string $connectionType
     * @param int $chunkSize
     * @throws LoggerException
     */
    public function __construct(
        string $host,
        int $port,
        string $connectionType = self::CONNECTION_TYPE_TCP,
        int $chunkSize = 262144
    ) {
        $this->makeConnection($host, $port, $connectionType, $chunkSize);
        $this->publisher = new Publisher($this->connection);
    }

    /**
     * @param string $host
     * @param int $port
     * @param string $connectionType
     * @param int $chunkSize
     * @throws LoggerException
     */
    private function makeConnection(
        string $host,
        int $port,
        string $connectionType = self::CONNECTION_TYPE_TCP,
        int $chunkSize = 262144
    ) {
        switch ($connectionType) {
            case self::CONNECTION_TYPE_TCP:
                $this->connection = new TcpTransport($host, $port);
                break;
            case self::CONNECTION_TYPE_UDP:
                $this->connection = new UdpTransport($host, $port, $chunkSize);
                break;
            default:
                throw new LoggerException('InvalidConnectionType', 'graylog', [
                    'type' => $connectionType,
                ]);
        }
    }

    /**
     * @param LogMessage $data
     * @return void
     * @throws LoggerIsNotResponding
     */
    public function write(LogMessage $data): void
    {
        $msg = new Message();
        $msg->setLevel($data->getLevel())
            ->setShortMessage($data->getMsg())
            ->setFullMessage($this->decorate($data->getData()))
            ->setFacility($data->getKey())
            ->setTimestamp(time())
            ->setFile($data->getFile())
            ->setLine($data->getLine());
        try {
            $this->publisher->publish($msg);
        } catch (\Exception $exception) {
            throw new LoggerIsNotResponding($exception->getMessage(), 'graylog', [
                'msg' => $msg->toArray(),
            ], $exception, $exception->getCode());
        }
    }
}