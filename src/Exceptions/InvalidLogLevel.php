<?php
namespace Skipper\Logger\Exceptions;

use Skipper\Exceptions\DomainException;
use Skipper\Exceptions\Error;
use Throwable;

final class InvalidLogLevel extends DomainException
{
    public function __construct(
        array $context = [],
        Throwable $previous = null,
        int $code = 0
    ) {
        parent::__construct('Invalid log level', 'logger.loglevel', $context, $previous, $code);

        $this->errors = [];
        $this->addError(new Error('Invalid log level', 'invalidParameter', 'logger.loglevel'));
    }
}